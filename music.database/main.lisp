(defvar *db* nil)


(defun make-cd (title artist rating ripped)
  (list :title title
        :artist artist
        :rating rating
        :ripped ripped))


(defun add-cd (cd)
  (push cd *db*))


(defun dump (db)
  (format t "~{~{~a: ~10t~a~%~}~%~}" db))


(defun dump-db ()
  "Pretty print the database.

   ~{   -> format will expect the handle the next argument as a list.
           and will loop over it, until the next }~ is read.

   ~10t -> move 10 columns forward (10 spaces)

    ~a  -> 'aesthetic' directive, put the consumed argument into
           human readable form
"
  (dump *db*))


(defun prompt-read (prompt)
  (format *query-io* "~a: " prompt)
  (force-output *query-io*)
  (read-line *query-io*))


(defun prompt-for-cd ()
  (make-cd
   (prompt-read "Title")
   (prompt-read "Artist")
   (or (parse-integer (prompt-read "Rating") :junk-allowed t) 0)
   (y-or-n-p "Ripped [y/n]: ")))


(defun add-cds-loop ()
  (loop (add-cd (prompt-for-cd))
        (if (not (y-or-n-p "Add another CD? [y/n]: ")) (return))))


(defun save-db (filename)
  "Save the database to FILENAME.

   Unlike format, print writes data in a form, that can be
   written by the lisp-reader afterwards.
  "
  (with-open-file (out filename
                       :direction :output
                       :if-exists :supersede)
    (with-standard-io-syntax
      (print *db* out))))


(defun load-db (filename)
  "Load the database from a file at FILENAME.

   read is the same lisp expression reader as used by the REPL.
  "
  (with-open-file (in filename
                      :direction :input)
    (with-standard-io-syntax
      (setf *db* (read in)))))


(defun select-by-artist (artist)
  "Select all items from the database that match the given ARTIST name.

   #'  ->  Use the function with the following name
           (apply #'+ l) ==  (apply (function +) l)
  "
  (dump
   (remove-if-not
    #'(lambda (cd) (equal (getf cd :artist) artist))
    *db*)))


(defun select (selector-fn)
  (dump (remove-if-not selector-fn *db*)))

;; (defun artist-selector (artist)
;;   #'(lambda (cd) (equal (getf cd :artist) artist)))

(defun where (&key title artist rating (ripped nil ripped-p))
  #'(lambda (cd)
      (and
       (if title    (equal (getf cd :title)  title)  t)
       (if artist   (equal (getf cd :artist) artist) t)
       (if rating   (equal (getf cd :rating) rating) t)
       (if ripped-p (equal (getf cd :ripped) ripped) t))))


(defun update (selector-fn &key title artist rating (ripped nil ripped-p))
  (setf *db*
        (mapcar
         #'(lambda (row)
             (when (funcall selector-fn row)
               (if title    (setf (getf row :title)  title))
               (if artist   (setf (getf row :artist) artist))
               (if rating   (setf (getf row :rating) rating))
               (if ripped-p (setf (getf row :ripped) ripped)))
             row)
         *db*)))


(defun delete-rows (selector-fn)
  (setf *db* (remove-if selector-fn *db*)))


(defmacro backwards (expr) (reverse expr))

(defun make-comparison-expr (field value)
  (list 'equal (list 'getf 'cd field) value))

;; ` or ' stop the followed expression from being evaluated
`(1 2 3)

;; You can use comma-prefixed expression in back-quoted expressions
;; to evaluate parts of it
`(1 2 ,(+ 1 2))

;; It's similar to string templates, but with code
(defun make-comparison-expr (field value)
  `(equal (getf cd ,field) ,value))
